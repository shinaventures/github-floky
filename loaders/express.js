const express = require('express');
const path = require('path');
const { errorHandler, validateGithubRepoUrl } = require('../middlewares');
const { homeController, repoController } = require('../controllers');

const loadExpress = ({ expressApp: app }) => {
    app.use('/static', express.static(path.join(process.cwd(), 'public')));

    app.get('/', homeController);
    app.get('/repo', validateGithubRepoUrl, repoController);

    app.use(errorHandler);
};

module.exports = loadExpress;