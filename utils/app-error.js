class AppError extends Error {
    constructor(message, httpStatusCode = 500) {
        super(message);

        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, AppError);
        }

        this.name = 'AppError';
        this.statusCode = httpStatusCode;
        this.date = new Date();
    }
}

module.exports = AppError;