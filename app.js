const express = require('express');

const startServer = async () => {
    const app = express(), port = 3000;

    require('./loaders/express')({ expressApp: app });

    app.listen(port, () => {
        console.log(`App is listening on port ${port}`);
    });
};

startServer();