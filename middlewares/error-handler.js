module.exports = (error, req, res, next) => {
    const genericStatusCode = 500
    const description = 'failure';

    if (error.name === 'AppError') {
        const { statusCode, message } = error;

        return res.status(statusCode).json({
            message,
            statusCode,
            description
        });
    }

    return res.status(genericStatusCode).json({
        message: 'The server encountered an error an error when processsing your request',
        statusCode: genericStatusCode,
        description
    });
};