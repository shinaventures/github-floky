const AppError = require('../utils/app-error');

module.exports = (req, res, next) => {
    let repoUrl;

    try {
        repoUrl = req.query.repoUrl || '';
        const urlParts = repoUrl.split('/');

        if (urlParts.length === 5 && isValidUrl(urlParts)) {
            const [repoOwner, repoName] = urlParts.slice(3);
            req.body = { repoOwner, repoName };

            return next();
        }

        throw new AppError(`The url ${repoUrl} is not a valid GitHub repository url`, 403);
    } catch (error) { next(error); }
};

function isValidUrl(urlParts) {
    const validBaseUrl = ['github.com', 'www.github.com'];
    return validBaseUrl.includes(urlParts[2]);
}