const GitHubServices = require('./services');

const homeController = (req, res, next) => res.sendFile(`${__dirname}/public/index.html`);

const repoController = async (req, res, next) => {
    try {
        const { repoOwner, repoName } = req.body;

        let pullRequests = await GitHubServices.fetchPullRequests(repoOwner, repoName);
        pullRequests = await GitHubServices.addPullRequestsCommitsAndCommentsNumber(pullRequests);

        return res.json({ pullRequests });
    }
    catch (error) { next(error); }
};

module.exports = { homeController, repoController };