const rp = require('request-promise');
const config = require('./config');
const AppError = require('./utils/app-error');

class GitHubServices {
    static async fetchPullRequests(repoOwner, repoName) {
        try {
            let repoHasMorePullRequests = true;
            let currentPage = 1;
            const resultPerPage = 50;
            const pullRequests = [];

            let options = {
                ...config.requestOptions,
                resolveWithFullResponse: true
            };

            for (; repoHasMorePullRequests;) {
                let url = `${config.githubApiEndpoint}/${repoOwner}/${repoName}/pulls`;
                url += `?page=${currentPage}&per_page=${resultPerPage}`;
                options.uri = url;

                let { headers, body } = await rp(options);
                pullRequests.push(...body);

                repoHasMorePullRequests = headers.link !== undefined && headers.link.includes('next');
                currentPage += 1;
            }

            return pullRequests;
        } catch (error) {
            throw new AppError(
                'An error when trying to get the pull requests from github',
                503
            );
        }
    }

    static async addPullRequestsCommitsAndCommentsNumber(pullRequests) {
        try {
            const result = await Promise.all(pullRequests.map(async (pullRequest) => {
                const options = {
                    uri: pullRequest.url,
                    ...config.requestOptions,
                };

                const singlePullRequest = await rp(options);

                return {
                    ...pullRequest,
                    commitsCount: singlePullRequest.commits,
                    commentsCount: singlePullRequest.comments,
                };
            }));

            return result;
        } catch (error) {
            throw new AppError(
                'An error occured when trying to get the number of commits and comments for each pull requests',
                503
            );
        }
    }
}

module.exports = GitHubServices;