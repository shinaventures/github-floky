# github-floky

A simple node js app to fetch details on a github repo and display it.

--instrcuctions
1. clone the repository from git hub using ssh or https
2. run 'npm install' to install all depencies
3. run 'npm start' to run application
    a. application runs on port 3000
    b. go to http://localhost:3000 in your browser
    c. application home page should now dsiplay
4. paste a github repo url in the input box and click the 'Get summary' button to see the repo details
