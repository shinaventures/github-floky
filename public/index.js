/*************** UI CONTROLLER ********************/
const UIController = (function () {
    const domStrings = {
        urlInput: '.form-control',
        summaryButton: 'button',
        tableBody: 'tBody',
        loaderDiv: '.loader-div'
    };

    const tableElementBody = document.querySelector(domStrings.tableBody);
    const loaderElement = document.querySelector(domStrings.loaderDiv);

    return {
        getDomStrings: function () {
            return domStrings;
        },

        clearTableBody: function () {
            tableElementBody.innerHTML = '';
        },

        startLoader: function () {
            loaderElement.classList.add('loader');
        },

        clearLoader: function () {
            loaderElement.classList.remove('loader');
        },

        clearUrlInput: function () {
            const urlInput = document.querySelector(domStrings.urlInput);
            urlInput.value = '';
        },

        displayPullRequests: function (pullRequests) {
            pullRequests.forEach(pullRequest => {
                const { pullNumber, commentsCount, commitsCount, createdTime, user } = pullRequest;

                let tableRowString = `<tr><th scope="row">${pullNumber}</th><td>${user}</td><td>${commitsCount}</td>`;
                tableRowString += `<td>${commentsCount}</td><td>${moment(createdTime).fromNow()}</td></tr>`;

                tableElementBody.insertAdjacentHTML('beforeend', tableRowString);
            });
        }
    };
})();

/*************** DATA CONTROLLER ********************/
const DataController = (function () {
    return {
        getGitHubRepoDetails: async function (repoUrl) {
            const url = `/repo?githuburl=${repoUrl}`;

            const response = await fetch(url);
            const repoDetails = await response.json();

            return repoDetails;
        }
    }
})();

/*************** APP CONTROLLER ********************/
const AppController = (function (UICtrl, DataCtrl) {
    const domStrings = UICtrl.getDomStrings();

    function setupEventListeners() {
        const buttonElement = document.querySelector(domStrings.summaryButton);
        const inputElement = document.querySelector(domStrings.urlInput);

        buttonElement.addEventListener('click', async (evt) => {
            evt.preventDefault();
            const githubRepoUrl = inputElement.value;

            if (githubRepoUrl.length > 0) {
                UICtrl.startLoader();

                const repoDetails = await DataCtrl.getGitHubRepoDetails(githubRepoUrl);
                const { pagination, pullRequests } = repoDetails;

                UICtrl.clearTableBody();
                UICtrl.clearUrlInput();
                UICtrl.clearLoader();

                UICtrl.displayPullRequests(pullRequests);
            }
        });
    }

    return {
        init: function () {
            setupEventListeners()
        }
    };
})(UIController, DataController);


/*************** Start ********************/
AppController.init();