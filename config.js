const githubApiEndpoint = 'https://api.github.com/repos';

const requestOptions = {
    method: 'GET',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true
};

module.exports = { requestOptions, githubApiEndpoint };